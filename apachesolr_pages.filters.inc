<?php

/**
 * Shorthand function for instantiating filter objects.
 * @param $id
 * @return null|object
 */
function as_pages_filter($id) {
  $args = func_get_args();
  array_shift($args);
  $filter = _apachesolr_pages_filters($id);
  if ($filter && isset($filter['handler']) && class_exists($filter['handler'])) {
    $reflect = new ReflectionClass($filter['handler']);
    return $reflect->newInstanceArgs($args);
  }
  else {
    return NULL;
  }
}

/**
 * Returns a cached list of all filter types.
 * @param bool $id
 * @return array|null
 */
function _apachesolr_pages_filters($id = FALSE) {
  $filters = &drupal_static(__FUNCTION__, FALSE);
  if (!$filters) {
    $filters = module_invoke_all('apachesolr_pages_filters');
    module_load_include('inc', 'apachesolr_pages', 'filters/apachesolr_filter');
    foreach ($filters as $key => &$filter) {
      _apachesolr_pages_load_handler($key, $filter);
    }
  }
  if ($id) {
    if (isset($filters[$id])) {
      return $filters[$id];
    }
    else {
      return NULL;
    }
  }
  return $filters;
}


/**
 * Implements hook_apachesolr_pages_filters().
 */
function apachesolr_pages_apachesolr_pages_filters() {
  $path = drupal_get_path('module', 'apachesolr_pages') . '/filters';
  $filters = array(
    'submit' => array(
      'handler' => 'apachesolr_filter_submit',
    ),
    'reset' => array(
      'handler' => 'apachesolr_filter_reset',
    ),
    'text' => array(
      'handler' => 'apachesolr_filter_text',
    ),
    'entity_reference' => array(
      'handler' => 'apachesolr_filter_entity_reference',
    ),
    'options' => array(
      'handler' => 'apachesolr_filter_options',
    ),
    'taxonomy_term' => array(
      'handler' => 'apachesolr_filter_taxonomy_term',
    ),
    'group' => array(
      'handler' => 'apachesolr_filter_group',
    ),
    'date' => array(
      'handler' => 'apachesolr_filter_date',
    ),
  );
  foreach ($filters as &$filter) {
    $filter['path'] = $path;
  }
  return $filters;
}

/**
 * Hierarchical taxonomy term filter ajax callback function.
 * @param $vid
 */
function apachesolr_pages_ajax_hierarchical($vid) {
  $tid = empty($_GET['id']) ? 0 : $_GET['id'];

  $o = array();
  $tree = taxonomy_get_tree($vid, $tid, 1);
  foreach ($tree as $term) {
    $o[$term->weight] = array('id' => $term->tid, 'name' => $term->name);
  }
  drupal_json_output($o);
}

/**
 * Helper function to return vocabulary info by its machine name.
 * @param bool $machine_name
 * @return array|null
 */
function _apachesolr_pages_taxonomy_vocabularies($machine_name = FALSE) {
  $vocabularies = &drupal_static(__FUNCTION__);
  if (empty($vocabularies)) {
    $vocabularies = array();
    foreach (entity_load('taxonomy_vocabulary') as $v) {
      $vocabularies[$v->machine_name] = $v;
    }
  }
  if ($machine_name && isset($vocabularies[$machine_name])) {
    return $vocabularies[$machine_name];
  }
  else {
    if ($machine_name) {
      return NULL;
    }
  }
  return $vocabularies;
}


/**
 * Handles additional filter behaviours, @see apachesolr_filter::onInvalid
 * @param $page
 * @param array $build
 * @return string
 */
function apachesolr_pages_handle_filters($page, $build = array()) {
  foreach ($page->getFilters() as $filter) {
    $filter->setNewTitle($page);
    if (!$filter->isValid()) {
      switch ($filter->getOnInvalid()) {
        case NOT_FOUND:
          drupal_not_found();
          break;
        case NO_RESULTS:
          return apachesolr_pages_no_results($page);
          break;
        case REQUIRE_INPUT:
          return apachesolr_pages_require_input($page);
          break;
        case DISPLAY_ALL:
          break;
      }
    }
  }
  return NULL;
}
