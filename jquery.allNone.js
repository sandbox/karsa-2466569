(function ($) {

  $.fn.allNone = function () {

    var toggleNames = {
      all: Drupal.t('Select all'),
      none: Drupal.t('Select None'),
    };

    return this.each(function () {
      var $widget = $(this);
      var $checkboxes = $('input[type=checkbox]', $widget);

      var $toggle = $('<a class="apachesolr-filters-toggle" href="#"></a>')
        .click(function () {
          var $toggle = $(this);
          var isEmpty = $checkboxes.filter('input:checked').length == 0;

          if (isEmpty) {
            $checkboxes.filter(':not(:checked):not(:disabled)').attr('checked', 'checked').change();
            $toggle.text(toggleNames.none);
          }
          else {
            $checkboxes.filter(':checked:not(:disabled)').removeAttr('checked').change();
            $toggle.text(toggleNames.all);
          }
          return false;
        });
      if ($checkboxes.length > 2) $toggle.prependTo($widget);

      var updateText = function () {
        var isEmpty = $checkboxes.filter('input:checked').length == 0;
        $toggle.text(isEmpty ? toggleNames.all : toggleNames.none);
      };

      $checkboxes.change(updateText);

      updateText();
    });
  };

})(jQuery);
