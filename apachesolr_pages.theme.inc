<?php

/**
 * Implements hook_theme().
 */
function apachesolr_pages_theme() {
  $theme_path = drupal_get_path('module', 'apachesolr_pages') . '/theme';
  return array(
    'apachesolr_pages_page' => array(
      'template' => 'apachesolr-pages-page',
      'path' => $theme_path,
    ),
    'apachesolr_pages_results' => array(
      'template' => 'apachesolr-pages-page-results',
      'path' => $theme_path,
    ),
    'apachesolr_pages_page_table' => array(
      'template' => 'apachesolr-pages-page-table',
      'path' => $theme_path,
    ),
    'apachesolr_pages_contents' => array(
      'template' => 'apachesolr-pages-contents',
      'path' => $theme_path,
    ),
    'apachesolr_pages_content' => array(
      'template' => 'apachesolr-pages-content',
      'path' => $theme_path,
    ),
    'asp_facet' => array(
      'variables' => array('name' => NULL),
    ),
  );
}

/**
 * Themes a facet number.
 * @param $vars
 * @return string
 */
function theme_asp_facet($vars) {
  return '<span class="asp-facet-info asp-facet-' . $vars['id'] . '" data-facet-id="' . $vars['id'] . '">' . (!empty($vars['name']) ? '(' . $vars['name'] . ')' : '') . '</span>';
}

/**
 * Creates the basic build array for a search page, containing basic data.
 * @param $page
 * @param $query
 * @param bool $suggestions
 * @return array
 * @throws \Exception
 */
function apachesolr_pages_basic_page_build($page, $query, $suggestions = FALSE) {
  $build = array();
  $build['#id'] = $page->getId();
  $build['#page'] = $page;
  $build['#query'] = $query;
  $build['form'] = $page->getForm();
  $build['active_filters'] = $page->getActiveFilters();
  if ($suggestions) {
    $suggestions_output = array();
    $suggestions = get_object_vars($suggestions->suggestions);
    foreach ($page->getFilters() as $filter_id => $filter) {
      if (!$filter->hasField()) {
        $keyword = implode("\n", $filter->getValue());
        if ($suggestions) {
          $replacements = array();
          foreach ($suggestions as $word => $value) {
            $suggestion = $value->suggestion;
            $replacements[$word] = is_object($suggestion[0]) ? $suggestion[0]->word : $suggestion[0];
          }
          $suggested_keyword = strtr($keyword, $replacements);
          if ($keyword != $suggested_keyword) {
            $suggestions_output[$filter_id][] = $suggested_keyword;
          }
        }
      }
    }
    if (!empty($suggestions_output)) {
      $suggested = '';
      $query = array();
      foreach ($suggestions_output as $filter => $suggested_keywords) {
        $suggested = $suggested_keywords[0];
        $query[$filter] = $suggested_keywords[0];
      }
      $build['suggestions'] = theme('apachesolr_search_suggestions', array('links' => array(l($suggested, $page->getPath(), array('query' => $query)))));
    }
  }
  return $build;
}

/**
 * Themes a search page using its theme function.
 * @param $page
 * @param $build
 * @return string
 * @throws \Exception
 */
function apachesolr_pages_basic_page_theme($page, $build) {
  drupal_set_title($page->getTitle());
  $build['attributes_array'] = $page->getAttributes();
  $page->alterBuild($build);
  drupal_alter("apachesolr_pages_page_view", $build);
  return theme($page->getTheme(), $build);
}

/**
 * Creates a "No results" search page.
 * @param $page
 * @param bool $query
 * @param bool $response
 * @param bool $suggestions
 * @param bool $facet_counts
 * @param bool $headers
 * @param bool $footers
 * @return string
 */
function apachesolr_pages_no_results($page, $query = FALSE, $response = FALSE, $suggestions = FALSE, $facet_counts = FALSE, $headers = FALSE, $footers = FALSE) {
  $build = apachesolr_pages_basic_page_build($page, $query, $suggestions);
  $build['no_results'] = TRUE;
  $build['results'] = array();
  $build['results']['pager'] = array();
  $build['headers'] = $headers;
  $build['footers'] = $footers;
  $build['results']['results']['#markup'] = $page->getNoResults();
  $build['results']['results']['#prefix'] = '<div class="no-results">';
  $build['results']['results']['#suffix'] = '</div>';
  $build['items'] = array();
  return apachesolr_pages_basic_page_theme($page, $build);
}

/**
 * Creates a "Requires input" search page.
 * @param $page
 * @param bool $query
 * @param bool $headers
 * @param bool $footers
 * @return string
 */
function apachesolr_pages_require_input($page, $query = FALSE, $headers = FALSE, $footers = FALSE) {
  $build = apachesolr_pages_basic_page_build($page, $query);
  $build['empty_input'] = TRUE;
  $build['results'] = array();
  $build['results']['pager'] = array();
  $build['headers'] = $headers;
  $build['footers'] = $footers;
  $build['results']['results']['#markup'] = $page->getRequireInputMessage();
  $build['results']['results']['#prefix'] = '<div class="require-input">';
  $build['results']['results']['#suffix'] = '</div>';
  $build['items'] = array();
  return apachesolr_pages_basic_page_theme($page, $build);
}

/**
 * Processes the results of the solr query and returns a search page build array.
 * @param $page
 * @param $query
 * @param $response
 * @param $suggestions
 * @param $facet_counts
 * @param bool $headers
 * @param bool $footers
 * @return string
 * @throws \Exception
 */
function apachesolr_pages_default_page($page, $query, $response, $suggestions, $facet_counts, $headers = FALSE, $footers = FALSE) {
  $build = apachesolr_pages_basic_page_build($page, $query, $suggestions);
  $build['empty_input'] = FALSE;
  $build['no_results'] = FALSE;

  $build['headers'] = $headers;
  $build['footers'] = $footers;

  $items = array();

  $total = $response->numFound;

  $results_theme = $page->getResultsTheme();
  $build['results_classes'] = implode(' ', $page->getResultsClass());
  if ($results_theme) {
    $build['items'] = array();
    foreach ($response->results as $i => $doc) {
      $stripe = ($i % 2 == 0 ? 'even' : 'odd');
      $classes = $page->getRowClass();
      $classes[] = $stripe;
      $classes[] = 'apachesolr-row-' . $stripe;

      $build['items'][] = array(
        'content' => $page->renderRow($doc),
        'class' => implode(' ', $classes),
      );
    }

    $build['results']['results'] = theme($results_theme, $build);
  }
  else {
    foreach ($response->results as $i => $doc) {
      $items[] = $page->renderRow($doc);
    }
    $build['items'] = $items;
  }

  $build['results']['pager']['#markup'] = theme('pager', array('quantity' => $total));
  return apachesolr_pages_basic_page_theme($page, $build);
}
