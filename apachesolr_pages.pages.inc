<?php

/**
 * Returns a list of all defined search pages or a certain page if an id is given.
 */
function _apachesolr_pages_pages($id = NULL) {
  $pages = &drupal_static(__FUNCTION__);
  _apachesolr_pages_page_types();
  if (empty($pages)) {
    $pages = module_invoke_all('apachesolr_pages_pages');
  }
  if (!empty($id) && $id !== NULL && $id && !empty($pages) && !empty($pages[$id])) {
    return $pages[$id];
  }
  else {
    if (!empty($id) && $id !== NULL) {
      return FALSE;
    }
  }
  return $pages;
}

/**
 * Returns a page by its id.
 * @param $page_id
 * @param bool $page_info
 * @return mixed
 */
function apachesolr_pages_get_page($page_id, $page_info = FALSE) {
  $pages = &drupal_static(__FUNCTION__, array());
  if (empty($pages[$page_id])) {
    if (!$page_info) {
      $page_info = _apachesolr_pages_pages($page_id);
    }
    $pages[$page_id] = module_invoke($page_info['module'], 'apachesolr_pages_page', $page_id);
    drupal_alter('apachesolr_pages_page', $pages[$page_id]);
  }
  return $pages[$page_id];
}


/**
 * Returns the search page object for a path.
 * Returns FALSE if the path does not belong to a solr search page.
 * @param $path
 * @return bool
 */
function _apachesolr_pages_page_for_path($path) {
  $ancestors = menu_get_ancestors(explode('/', $path));
  foreach ($ancestors as $ancestor) {
    $item = db_select('menu_router', 'r')
      ->fields('r')
      ->condition('path', $ancestor)
      ->execute()
      ->fetchAssoc();
    if ($item) {
      break;
    }
  }
  if (@$item['page_callback'] == 'apachesolr_pages_page') {
    $arguments = $item['page_arguments'];
    if (!is_array($arguments)) {
      $arguments = unserialize($arguments);
      $page = apachesolr_pages_get_page($arguments[0]);
      return $page;
    }
  }
  return FALSE;
}