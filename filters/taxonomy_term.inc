<?php

// Parser types
define('BY_NAME', 1);
define('BY_TID', 2);

/**
 * Taxonomy term filter.
 */
class apachesolr_filter_taxonomy_term extends apachesolr_filter_options {
  /**
   * The taxonomy vocabulary to use.
   * @var bool
   */
  var $vocabulary = FALSE;
  /**
   * The parent term to generate the tree from.
   * @var int
   */
  var $parent = 0;
  /**
   * The depth the generated tree should be limited to.
   * @var
   */
  var $depth = INF;
  /**
   * Whether only leaves should be selectable or not.
   * @var bool
   */
  var $selectableParents = FALSE;
  /**
   * The parser method to use when receiving parameters.
   * @var int
   */
  var $parser = BY_TID;
  /**
   * Whether or not to replace dashes in term name parameters.
   * @var bool
   */
  var $replaceDashes = TRUE;
  /**
   * Whether or not the option tree has been loaded.
   * @var bool
   */
  var $options_loaded = FALSE;

  /**
   * @param array $field
   * @param bool $vocabulary
   * @param array $options
   * @param int $parent
   * @param $depth
   * @param bool $selectableParents
   * @param bool $title
   */
  function __construct($field, $vocabulary, $options = array(), $parent = 0, $depth = INF, $selectableParents = FALSE, $title = FALSE) {
    parent::__construct($options, $field, $title);
    $this->setParent($parent);
    $this->setDepth($depth);
    $this->setVocabulary($vocabulary);
    $this->selectableParents = $selectableParents;
  }

  /**
   * Setter function for the parent attribute.
   * @param $parent
   * @return $this
   */
  function setParent($parent) {
    $this->parent = $parent;
    return $this;
  }

  /**
   * Setter function for the vocabulary attribute.
   * @param $vocabulary
   * @return $this
   */
  function setVocabulary($vocabulary) {
    $this->vocabulary = _apachesolr_pages_taxonomy_vocabularies($vocabulary);
    if ($this->title === FALSE) {
      $this->title = $this->vocabulary->name;
    }
    return $this;
  }

  /**
   * @see apachesolr_filter::getActiveFilterValues()
   */
  function getActiveFilterValues() {
    $tmp = array();
    foreach ($this->getValue() as $key => $val) {
      $option = $this->getOption($val);
      if (!empty($option)) {
        $name = $option->getName();
      }
      else {
        $name = taxonomy_term_load($val)->name;
      }
      $tmp[$val] = $name;
    }
    return $tmp;
  }

  /**
   * Setter function for the parser attribute.
   * @param $parser
   * @return $this
   */
  function setArgumentParser($parser) {
    $this->parser = $parser;
    return $this;
  }

  /**
   * Setter function for the replaceDashes attribute
   * @param $replaceDashes
   * @return $this
   */
  function setReplaceDashes($replaceDashes) {
    $this->replaceDashes = $replaceDashes;
    return $this;
  }

  /**
   * @see apachesolr_filter::parseValue()
   */
  function parseValue($value) {
    $values = parent::parseValue($value);
    foreach ($values as $key => $val) {
      switch ($this->parser) {
        case BY_NAME:
          if ($this->replaceDashes) {
            $terms = taxonomy_get_term_by_name(str_replace('-', ' ', $val), $this->vocabulary ? $this->vocabulary->machine_name : NULL);
          }
          else {
            $terms = taxonomy_get_term_by_name($val, $this->vocabulary->machine_name);
          }
          $term = FALSE;
          if (count($terms) == 1) {
            $term = reset($terms);
          }
          break;
        case BY_TID:
        default:
          $term = taxonomy_term_load($val);
          break;
      }
      if ($term) {
        $values[$key] = $term->tid;
      }
      else {
        unset($values[$key]);
      }
    }
    return $values;
  }

  /**
   * @see apachesolr_filter::extendFacetParams()
   */
  function extendFacetParams(&$q, &$fq) {
    $this->loadOptions();
    parent::extendFacetParams($q, $fq);
  }

  /**
   * Loads the option tree
   */
  function loadOptions() {
    if (!$this->options_loaded) {
      switch ($this->type) {
        case 'hierarchical':
          $options = array();
          foreach ($this->getValue() as $tid) {
            $options[$tid] = taxonomy_term_load($tid)->name;
          }
          break;
        default:
          $tree = taxonomy_get_tree($this->vocabulary->vid, $this->parent, $this->depth);
          $options = $this->loadSuboptions($this->parent, $tree);
          $this->setOptions($options);
          break;
      }
    }
    $this->options_loaded = TRUE;
  }

  /**
   * Option tree loading helper function.
   * @param $tid
   * @param $tree
   * @return array
   */
  function loadSuboptions($tid, $tree) {
    $options = array();
    foreach ($tree as $term) {
      if (in_array($tid, $term->parents)) {
        $options[$term->tid] = array(
          'name' => $term->name,
          'selectable' => $this->selectableParents,
          'options' => $this->loadSuboptions($term->tid, $tree),
        );
      }
    }
    return $options;
  }

  /**
   * @see apachesolr_filter::element()
   */
  function element() {
    $this->loadOptions();
    return parent::element();
  }
}
