<?php

/**
 * A simple from-to date filter.
 */
class apachesolr_filter_date extends apachesolr_filter {
  use apachesolr_filter_collapsible;

  /**
   * Whether the filter should use HTML5 placeholders.
   * @var bool
   */
  var $placeholder = FALSE;
  /**
   * The starting year of the filter.
   * @var bool|int
   */
  var $from = FALSE;
  /**
   * The ending year of the filter.
   * @var bool|int
   */
  var $to = FALSE;

  /**
   * A simple constructor.
   * @param bool $field SOlr field.
   * @param bool $title Filter label.
   * @param int $from Starting year.
   * @param int $to Ending year.
   */
  function __construct($field = FALSE, $title = FALSE, $from = FALSE, $to = FALSE) {
    $this->from = $from ? $from : date('Y') - 5;
    $this->to = $to ? $to : date('Y') + 5;
    parent::__construct($field, $title);
  }

  /**
   * @see apachesolr_filter::element()
   */
  function element() {
    $this->class[] = 'apachesolr-date-interval';
    $element = parent::element();

    $element['#type'] = 'fieldset';
    $element['#tree'] = TRUE;
    $element['#title'];
    $element['#collapsed'] = $this->collapsed && (!$this->hasValue() || !$this->expand_on_input);
    $element['#collapsible'] = $this->collapsible;

    $value = $this->getValue();
    $element['min'] = $this->dateItem($value['min'], 'from');
    $element['separator'] = array(
      '#type' => 'markup',
      '#markup' => '<div class="separator">-</div>',
    );
    $element['max'] = $this->dateItem($value['max'], 'to');

    $element['clear'] = array(
      '#type' => 'markup',
      '#markup' => '<div class="clear"></div>',
    );

    if ($this->placeholder) {
      $element['#attributes']['placeholder'] = $this->title;
      unset($element['#title']);
    }

    return $element;
  }

  /**
   * Returns a date item for one part of the form element.
   * @param $value
   * @param array $type
   * @return array
   */
  function dateItem($value, $type = array()) {
    $form = array(
      '#type' => 'container',
      '#tree' => TRUE,
      '#title' => t(ucfirst($type)),
      '#attributes' => array(
        'class' => array($type),
      ),
      'title' => array(
        '#type' => 'item',
        '#title' => t(ucfirst($type)),
      ),
    );
    $opts = array();
    $opts['year'] = array('' => '- ' . t('Year') . ' -');
    for ($y = $this->to; $y >= $this->from; --$y) {
      $opts['year'][$y] = $y;
    }
    $opts['month'] = date_month_names();
    unset($opts['month'][0]);
    $opts['day'] = array();
    for ($d = 1; $d <= 31; ++$d) {
      $opts['day'][$d] = $d;
    }

    foreach (array('year', 'month', 'day') as $item) {
      $default = FALSE;
      if ($item == 'month' || $item == 'day') {
        $default = 1;
      }
      $form[$item] = array(
        '#type' => 'select',
        '#title' => t(ucfirst($item)),
        '#options' => $opts[$item],
        '#default_value' => isset($value[$item]) ? $value[$item] : $default,
        '#disabled' => $item != 'year' && empty($value['year']),
        '#attributes' => array(
          'class' => array($item),
          'data-placeholder' => '- ' . t(ucfirst($item)) . ' -',
        ),
      );
    }
    return $form;
  }

  /**
   * @see apachesolr_filter::parseValue()
   */
  function parseValue($value) {
    $value['min'] = array_filter($value['min']);
    $value['max'] = array_filter($value['max']);
    return array_filter($value);
  }

  /**
   * @see apachesolr_filter::buildQuery()
   */
  function buildQuery($value = NULL) {
    if (empty($value['min']) && empty($value['max'])) {
      $value = $this->getValue();
      if (empty($value)) {
        return NULL;
      }
    }
    $from = '*';
    $to = '*';
    if (!empty($value['min'])) {
      $from = $this->solrDateFromDate($value['min']);
    }
    if (!empty($value['max'])) {
      $to = $this->solrDateFromDate($value['max']);
    }
    return array(NULL, asp_between($this->field, $from, $to));
  }

  /**
   * Formats a date input as a Solr date string.
   */
  function solrDateFromDate($d) {
    return date('Y-m-d\TH:i:s\Z', mktime(0, 0, 0, $d['month'], $d['day'], $d['year']));
  }
}
