<?php

/**
 * Class apachesolr_filter_entity_reference
 */
class apachesolr_filter_entity_reference extends apachesolr_filter {
  /**
   * The entity_type the filter uses.
   * @var string
   */
  var $entity_type;
  /**
   * The bundles the filter uses.
   * @var array
   */
  var $bundle = array();

  /**
   * @param bool $field
   * @param bool $title
   * @param string $entity_type
   * @param array $bundle
   */
  function __construct($field = FALSE, $title = FALSE, $entity_type = 'node', $bundle = array()) {
    parent::__construct($field, $title);
    $this->fieldOperator = ASP_EXP_OR;
    $this->entity_type = $entity_type;
    $this->bundle = is_array($bundle) ? $bundle : array($bundle);
    $this->process = TRUE;
  }

  /**
   * @see apachesolr_filter::element()
   */
  function element() {
    $element = parent::element();

    $element['#type'] = 'textfield';
    $element['#default_value'] = $this->getValue();

    return $element;
  }

  /**
   * @see apachesolr_filter::process()
   */
  function process($value) {
    return '"' . $this->entity_type . ':' . $value . '"';
  }

  /**
   * @see apachesolr_filter::isValid()
   */
  function isValid() {
    foreach ($this->getValue() as $key => $val) {
      $entity = entity_load_single($this->entity_type, $val);
      if ($entity) {
        if (!empty($this->bundle)) {
          list($entity_id, ,$bundle) = entity_extract_ids($this->entity_type, $entity);
          if (!in_array($bundle, $this->bundle)) {
            return FALSE;
          }
        }
      }
      else {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * @see apachesolr_filter::getActiveFilterValues()
   */
  function getActiveFilterValues() {
    $tmp = array();
    foreach ($this->getValue() as $key => $val) {
      $entity = entity_load_single($this->entity_type, $val);
      if ($entity) {
        $tmp[$key] = entity_label($this->entity_type, $entity);
      }
    }
    return $tmp;
  }
}
