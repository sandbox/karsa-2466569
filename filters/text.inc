<?php

/**
 * Simple textfield filter.
 */
class apachesolr_filter_text extends apachesolr_filter {
  /**
   * Whether or not the filter title should be displayed using an HTML5
   * placeholder attribute instead of the default form element title.
   *
   * @var bool
   */
  var $placeholder = FALSE;

  /**
   * @param bool $field
   * @param bool $title
   * @param bool $placeholder
   */
  function __construct($field = FALSE, $title = FALSE, $placeholder = FALSE) {
    $this->placeholder = $placeholder;
    parent::__construct($field, $title);
  }

  /**
   * @see apachesolr_filter::element()
   */
  function element() {
    $element = parent::element();

    $element['#type'] = 'textfield';
    $element['#default_value'] = $this->getValue();

    if ($this->placeholder) {
      $element['#attributes']['placeholder'] = $this->title;
      unset($element['#title']);
    }

    return $element;
  }
}