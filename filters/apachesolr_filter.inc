<?php

/**
 * Class apachesolr_filter_collapsible
 *
 * New PHP5.4 trait can adds fieldset collapsible functionality to an
 * apachesolr pages filter.
 * The filter still needs to implement the fieldset form element inside its
 * element() method.
 */
trait apachesolr_filter_collapsible {
  /**
   * @var bool Whether the filter should be collapsible.
   */
  var $collapsible = TRUE;
  /**
   * @var bool Whether the filter should be collapsed by default.
   */
  var $collapsed = TRUE;
  /**
   * @var bool Whether the filter should be expanded when there's any input.
   */
  var $expand_on_input = TRUE;

  /**
   * Setter function for the collapsed attribute.
   * @param $value
   * @return $this The filter object.
   */
  function setCollapsed($value) {
    $this->collapsed = $value;
    return $this;
  }

  /**
   * Setter function for the collapsible attribute.
   * @param $value
   * @return $this The filter object.
   */
  function setCollapsible($value) {
    $this->collapsible = $value;
    return $this;
  }

  /**
   * Setter function for the expand_on_input attribute.
   * @param $value
   * @return $this
   */
  function setExpandOnInput($value) {
    $this->expand_on_input = $value;
    return $this;
  }
}

/**
 * Class apachesolr_filter
 *
 * An abstract apachesolr pages filter.
 */
abstract class apachesolr_filter {
  /**
   * The label of the filter.
   * @var string
   */
  var $title;
  /**
   * The solr field this filter should be filtering on.
   * @var string
   */
  var $field;
  /**
   * The current value(s) of the filter.
   * Should only be set by setValue().
   * @var array
   */
  private $value = array();
  /**
   * The default value(s) of the filter.
   * @var array
   */
  var $default = array();
  /**
   * Whether the filter uses the default value or not, see setValue()
   * @var bool
   */
  var $uses_default = TRUE;
  /**
   * The solr operator the filter uses for filtering.
   * @var string
   */
  var $operator = '=';
  /**
   * The operator the filter uses if it has multiple values.
   * @var string
   */
  var $fieldOperator = 'AND';
  /**
   * Whether the filter is exposed or not.
   * @var bool
   */
  var $exposed = TRUE;
  /**
   * Additional CSS classes for the filter form element.
   * @var array
   */
  var $class = array();
  /**
   * Additional HTML attributes for the filter form element.
   * @var array
   */
  var $attributes = array();
  /**
   * A help text that should be displayed below the filter.
   * @var string
   */
  var $description;
  /**
   * A prefix for the filter form element.
   * @var string
   */
  var $prefix;
  /**
   * A suffix for the filter form element.
   * @var string
   */
  var $suffix;
  /**
   * Whether the filter should be displayed in the active filters.
   * @var bool
   */
  var $show_in_active_filters = TRUE;
  /**
   * Whether the filter's values should be grouped in the active filters.
   * @var bool
   */
  var $group_active_filter = FALSE;
  /**
   * Whether the filter's title should be omitted from the active filters.
   * @var bool
   */
  var $omit_active_filter_title = FALSE;
  /**
   * The unique machine name of the filter
   * This is used in request parameters and named arguments.
   * @var string
   */
  var $name;
  /**
   * The index of the argument filter, if the valueSource is INDEXED_ARGUMENT.
   * @var bool
   */
  var $index = FALSE;
  /**
   * Whether the filter's values should be preprocessed in solr queries.
   * @var bool
   */
  var $process = FALSE;
  /**
   * Whether the filter should show facets for available options.
   * @var bool
   */
  var $show_facets = TRUE;
  /**
   * The facet values mined from the solr query response.
   * @var
   */
  var $facets;
  /**
   * Whether the filter allows for multiple values as one.
   * If not FALSE, it should contain the separator the values are joined by.
   *
   * @var bool|string
   */
  var $allowMultiple = FALSE;
  /**
   * Whether the filter overrides the page title.
   * If not FALSE, it should contain the tokenized page title string.
   *
   * @var bool|string
   */
  var $overrideTitle = FALSE;
  /**
   * Whether the filter overrides the breadcrumb.
   * TODO: implement this feature.
   *
   * @var bool|string
   */
  var $overrideBreadcrumb = FALSE;
  /**
   * Whether this filter is required.
   *
   * @var bool
   */
  var $required = FALSE;
  /**
   * The action the search page should perform in case this filter is invalid.
   * Can be either:
   * - NOT_FOUND: deliver a "Page not found" error.
   * - DISPLAY_ALL: display all values (i.e. do not use this filter).
   * - NO_RESULTS: deliver an empty "No results" page.
   *
   * @var int
   */
  var $onInvalid = NOT_FOUND;
  /**
   * The source of the filter's value.
   * Can be either:
   * - REQUEST_PARAMETER: A simple request parameter.
   * - INDEXED_ARGUMENT: A standard Drupal argument.
   * - NAMED_ARGUMENT: An argument in the format of: "name:value(s)"
   * - NAMED_OR_REQUEST: A named argument with a fallback to request parameters.
   *
   * @var int
   */
  var $valueSource = REQUEST_PARAMETER;

  /**
   * A basic constructor for the apachesolr filter.
   */
  function __construct($field = FALSE, $title = FALSE, $default = array(), $exposed = TRUE, $process = FALSE) {
    $this->field = $field;
    $this->title = $title;
    $this->default = $default;
    $this->exposed = $exposed;
    $this->process = $process;
  }

  /**
   * Setter function for the filter name.
   */
  function setName($name) {
    $this->name = $name;
    return $this;
  }

  //region Filter values & defaults
  /**
   * Getter function for the $valueSource attribute.
   */
  function getValueSource() {
    return $this->valueSource;
  }

  /**
   * Setter function for the $valueSource attribute.
   */
  function setValueSource($valueSource) {
    $this->valueSource = $valueSource;
    return $this;
  }

  /**
   * Getter function for the value attribute.
   */
  function getValue() {
    if ($this->uses_default) {
      return $this->default;
    }
    return $this->value;
  }

  /**
   * Setter function for the value attribute.
   */
  final function setValue($value) {
    $this->uses_default = FALSE;
    $this->value = $this->parseValue($value);
    return $this;
  }

  /**
   * Returns whether the filter has a non-empty value.
   */
  function hasValue() {
    if ($this->uses_default) {
      return !empty($this->default);
    }
    return !empty($this->value);
  }

  /**
   * Parses the given filter value.
   */
  function parseValue($value) {
    if ($value==EMPTY_ARGUMENT_PLACEHOLDER || $value==array(EMPTY_ARGUMENT_PLACEHOLDER)) {
      return array();
    }
    if (is_array($value)) {
      return $value;
    }
    else {
      if (!empty($value)) {
        if ($this->allowMultiple) {
          return explode($this->allowMultiple, $value);
        }
        else {
          return array($value);
        }
      }
    }
    return array();
  }

  /**
   * Gets the default value of the filter.
   */
  function getDefault() {
    return $this->default;
  }

  /**
   * Sets the default value of the filter.
   */
  function setDefault($value) {
    if (is_array($value)) {
      $this->default = $value;
    }
    else {
      if (!empty($value)) {
        $this->default = array($value);
      }
      else {
        $this->default = array();
      }
    }
    return $this;
  }

  /**
   * Sets the multiple value separator.
   * FALSE, if the filter does not allow multiple values.
   * The separator if the filter allows multiple values.
   *
   * @param bool|string $separator
   * @return $this
   */
  function setAllowMultiple($separator) {
    $this->allowMultiple = $separator;
    return $this;
  }
  //endregion

  //region Arguments
  /**
   * Gets the index of the argument.
   * @return int
   */
  function getIndex() {
    return $this->index;
  }

  /**
   * Sets the index of the argument.
   * @param $index
   * @return $this
   */
  function setIndex($index) {
    $this->index = $index;
    return $this;
  }

  /**
   * Replaces the filter argument token in the given title.
   * @param $title
   * @return string
   */
  function replaceArg($title) {
    $values = $this->getActiveFilterValues();
    $tokens = array('!' . ($this->index + 1) => implode(', ', $values));
    return format_string($title, $tokens);
  }

  //endregion

  //region Form element generation (element, CSS class, HTML attributes, etc.)
  /**
   * Returns the search form element for the filter.
   * @return array
   */
  function element() {
    $element = array();
    $element['#prefix'] = $this->prefix;
    $element['#suffix'] = $this->suffix;
    $element['#description'] = $this->description;
    $element['#title'] = $this->title;
    $element['#attributes'] = $this->attributes;
    $element['#attributes']['class'] = $this->class;
    return $element;
  }

  /**
   * Getter function for the exposed attribute.
   * @return bool
   */
  function isExposed() {
    return $this->exposed;
  }

  /**
   * Setter function for the exposed attribute.
   * @param $exposed
   * @return $this
   */
  function setExposed($exposed) {
    $this->exposed = $exposed;
    return $this;
  }

  /**
   * Sets the title for the filter form element.
   * @param $title
   * @return $this
   */
  function setTitle($title) {
    $this->title = $title;
    return $this;
  }

  /**
   * Sets the description for the filter form element.
   * @param $description
   * @return $this
   */
  function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * Sets a prefix for the filter form element.
   * @param $prefix
   * @return $this
   */
  function setPrefix($prefix) {
    $this->prefix = $prefix;
    return $this;
  }

  /**
   * Sets a suffix for the filter form element.
   * @param $suffix
   * @return $this
   */
  function setSuffix($suffix) {
    $this->suffix = $suffix;
    return $this;
  }

  /**
   * Sets the attributes of the filter form element.
   * @param array $attributes The attributes array.
   * @return $this
   */
  function setAttributes($attributes) {
    if (is_array($attributes)) {
      $this->attributes = $attributes;
    }
    else {
      if (!empty($attributes)) {
        $this->attributes = array($attributes);
      }
      else {
        $this->attributes = array();
      }
    }
    return $this;
  }

  /**
   * Sets the CSS class array of the filter form element.
   * @param array $class The CSS class array.
   * @return $this
   */
  function setClass($class) {
    if (is_array($class)) {
      $this->class = $class;
    }
    else {
      if (!empty($class)) {
        $this->class = array($class);
      }
      else {
        $this->class = array();
      }
    }
    return $this;
  }

  /**
   * Adds a CSS class to the filter form element.
   * @param $class The CSS class name to add.
   * @return $this
   */
  function addClass($class) {
    if (is_array($class)) {
      $this->class = array_merge($class, $this->class);
    }
    else {
      $this->class [] = $class;
    }
    return $this;
  }
  //endregion

  //region Altering the page

  /**
   * Setter function for the overrideTitle attribute.
   * @param bool|string $title
   * @return $this
   */
  function setOverrideTitle($title) {
    $this->overrideTitle = $title;
    return $this;
  }

  /**
   * Sets a new title for the page based on the filter values.
   * @param $page
   * @return $this|void
   */
  function setNewTitle($page) {
    if (!$this->overrideTitle || !$this->hasValue() || !$this->isValid()) {
      return NULL;
    }
    foreach ($page->getArguments() as $argument) {
      $this->overrideTitle = $argument->replaceArg($this->overrideTitle);
    }
    $page->setTitle($this->overrideTitle);
    return $this;
  }

  /**
   * Getter function for the onInvalid attribute.
   * @return int
   */
  function getOnInvalid() {
    return $this->onInvalid;
  }

  /**
   * Setter function for the onInvalid attribute.
   * @param $onInvalid
   * @return $this
   */
  function setOnInvalid($onInvalid) {
    $this->onInvalid = $onInvalid;
    return $this;
  }

  /**
   * Setter function for the required attribute.
   * @param $required
   * @return $this
   */
  function setRequired($required) {
    $this->required = $required;
    return $this;
  }

  /**
   * Returns whether the state of the filter is valid.
   * @return bool
   */
  function isValid() {
    if ($this->required && !$this->hasValue()) {
      return FALSE;
    }
    return TRUE;
  }

  //endregion

  //region Active filters.
  /**
   * Getter funtion for the show_in_active_filters attribute.
   * @return bool Whether the filter should be shown in the active filters.
   */
  function getShowInActiveFilters() {
    return $this->show_in_active_filters;
  }

  /**
   * Setter funtion for the show_in_active_filters attribute.
   * @param $value Whether the filter should be shown in the active filters.
   * @return $this
   */
  function setShowInActiveFilters($value) {
    $this->show_in_active_filters = $value;
    return $this;
  }

  /**
   * Setter funtion for the omit_active_filter_title attribute.
   * @param $value Whether the active filter should include the filter title.
   * @return $this
   */
  function setOmitActiveFilterTitle($value) {
    $this->omit_active_filter_title = $value;
    return $this;
  }

  /**
   * Setter function for the group_active_filter attribute.
   * @param $value Whether the active filter values should be grouped.
   * @return $this
   */
  function setGroupActiveFilter($value) {
    $this->group_active_filter = $value;
    return $this;
  }

  /**
   * Returns an array of active filters associated with this filter.
   * @return array
   */
  function getActiveFilters() {
    $active = array();

    if ($this->omit_active_filter_title) {
      $label = '';
    }
    else {
      $label = $this->title . ': ';
    }
    $values = $this->getActiveFilterValues();
    if ($this->group_active_filter) {
      if (!empty($values)) {
        $active[] = array(
          'title' => $label . implode(', ', $values),
          'values' => array_keys($values)
        );
      }
    }
    else {
      foreach ($values as $key => $val) {
        $active[$key] = array(
          'title' => $label . $val,
          'values' => array($key)
        );
      }
    }

    return array($this->name => $active);
  }

  /**
   * Returns the formatted active filter values.
   * @return array
   */
  function getActiveFilterValues() {
    $tmp = array();
    foreach ($this->value as $val) {
      $tmp[$val] = $val;
    }
    return $tmp;
  }
  //endregion

  //region Facets
  /**
   * Setter function for the show_facets attribute.
   * @param $value Whether the facets should be shown or not.
   * @return $this
   */
  function setShowFacets($value) {
    $this->show_facets = $value;
    return $this;
  }

  /**
   * Loads the facet information from the solr query response.
   * @param $facets
   * @param $count
   */
  function loadFacetInfo($facets, $count) {
    if ($this->field) {
      $this->facets = @$facets->facet_fields->{$this->field};
    }
  }

  /**
   * Extends the solr query with facet parameters.
   * @param $q The solr query parameter.
   * @param $fq The solr field query parameters.
   */
  function extendFacetParams(&$q, &$fq) {
    if ($this->show_facets) {
      if ($this->field) {
        if ($this->hasValue() || $this->fieldOperator == ASP_EXP_AND) {
          $fq[] = $this->field;
        }
        else {
          $fq[] = '{!ex=' . $this->getQueryId() . '}' . $this->field;
        }
      }
    }
  }
  //endregion

  //region Solr query methods
  /**
   * Builds the filter query.
   * @param null $value An optional filter value, if empty, it uses getValue().
   * @return array|void The solr query and field query as a list, NULL if empty.
   */
  function buildQuery($value = NULL) {
    if ($value === NULL) {
      $value = $this->getValue();
      if (empty($value)) {
        return NULL;
      }
    }

    if ($this->process) {
      foreach ($value as &$val) {
        $val = $this->process($val);
      }
    }

    if (!$this->field) {
      return array(
        asp_native(implode(' ' . $this->fieldOperator . ' ', $value))->setTag($this->getQueryId()),
        NULL
      );
    }

    if (is_array($this->field)) {
      $exps = array();
      foreach ($this->field as $field_name) {
        $exps[] = asp_exp($field_name, $value, $this->operator, $this->fieldOperator);
      }
      return array(
        NULL,
        asp_logical($this->fieldOperator, $exps)->setTag($this->getQueryId()),
      );
    }
    return array(
      NULL,
      asp_exp($this->field, $value, $this->operator, $this->fieldOperator)->setTag($this->getQueryId()),
    );
  }

  /**
   * Sets the operator to use in filtering.
   * See the class apachesolr_page.
   * @param $operator
   * @return $this
   */
  function setOperator($operator) {
    $this->operator = $operator;
    return $this;
  }

  /**
   * Sets the field operator (AND|OR) to use when filtering for multiple values.
   * See the class apachesolr_page.
   * @param $fieldOperator The field operator (ASP_EXP_AND|ASP_EXP_OR)
   * @return $this
   */
  function setFieldOperator($fieldOperator) {
    $this->fieldOperator = $fieldOperator;
    return $this;
  }

  /**
   * Sets the process function of the filter.
   * @param $process The process function name.
   * @return $this
   */
  function setProcess($process) {
    $this->process = $process;
    return $this;
  }

  /**
   * Returns a processed filter value for the solr query.
   * Using this, you can alter filter values, i.e. make textfield filters
   * search for prefixes instead of exact words.
   *
   * @param $value The value to preprocess.
   * @return mixed The processed filter value.
   */
  function process($value) {
    return call_user_func($this->process, $value);
  }

  /**
   * Returns the unique query tag for this filter.
   * @return string
   */
  function getQueryId() {
    return 'filter_' . $this->name;
  }

  /**
   * Gets the Solr field attribute.
   * @return string
   */
  function getField() {
    return $this->field;
  }

  /**
   * Sets the Solr field attribute.
   * @param string $field The Solr field to use in the filter queries.
   * @return $this
   */
  function setField($field) {
    $this->field = $field;
    return $this;
  }

  /**
   * Returns whether the filter is based on a Solr field or is a generic search.
   * @return bool
   */
  function hasField() {
    return !empty($this->field);
  }
  //endregion

}