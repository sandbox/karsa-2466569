<?php

/**
 * Submit button "filter" class.
 */
class apachesolr_filter_submit extends apachesolr_filter {
  /**
   * The target page identifier (if any).
   * @var bool|string
   */
  var $page = FALSE;

  /**
   * Simple constructor.
   * @param bool $title
   * @param bool $page
   */
  function __construct($title, $page = FALSE) {
    $this->page = $page;
    parent::__construct(FALSE, $title);
  }

  /**
   * @see apachesolr_filter::element()
   */
  function element() {
    $element = parent::element();

    if ($this->page) {
      $target_page = apachesolr_pages_get_page($this->page);
      if ($target_page) {
        $element['#attributes']['data-target'] = url($target_page->getPath());
      }
    }

    $element['#type'] = 'submit';
    $element['#value'] = $this->title;

    return $element;
  }
}