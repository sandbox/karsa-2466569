<?php

/**
 * Class apachesolr_filter_group
 */
class apachesolr_filter_group extends apachesolr_filter {
  use apachesolr_filter_collapsible;

  /**
   * @see apachesolr_page::filters
   */
  var $filters = array();

  /**
   * @see apachesolr_filter::__construct()
   */
  function __construct($title, $description = FALSE, $fieldOperator = ASP_EXP_AND) {
    parent::__construct(FALSE, $title, NULL, TRUE);
    $this->fieldOperator = $fieldOperator;
  }

  /**
   * @see apachesolr_filter::extendFacetParams()
   */
  function extendFacetParams(&$q, &$fq) {
    foreach ($this->filters as $filter) {
      $filter->extendFacetParams($q, $fq);
    }
  }

  /**
   * @see apachesolr_filter::hasValue()
   */
  function hasValue() {
    foreach ($this->filters as $filter) {
      if ($filter->hasValue()) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * @see apachesolr_page::setFilters()
   */
  function setFilters($filters) {
    foreach ($filters as $name => $filter) {
      $filter->setName($name);
    }
    $this->filters = $filters;
    return $this;
  }

  /**
   * @see apachesolr_page::addFilter()
   */
  function addFilter($key, $filter) {
    $this->filters[$key] = $filter->setName($key);
    return $this;
  }

  /**
   * @see apachesolr_filter::element()
   */
  function element() {
    $element = parent::element();

    $element['#type'] = 'fieldset';
    $element['#collapsible'] = $this->collapsible;
    $element['#collapsed'] = $this->collapsed;

    _apachesolr_pages_form_add_filters($element, $this->filters);

    return $element;
  }

  /**
   * @see apachesolr_filter::buildFilter()
   * TODO: handle filters of valueSource argument inside groups as well.
   */
  function buildFilter(&$q, &$fq) {
    $subq = array();
    $subfq = array();
    foreach ($this->filters as $id => $subfilter) {
      if (!empty($_REQUEST[$id])) {
        $subfilter->setValue($_REQUEST[$id]);
      }
      $subfilter->buildFilter($subq, $subfq);
    }
    if ($this->operator == ASP_EXP_OR) {
      $q [] = '(' . implode(' OR ', $subq) . ')';
    }
    else {
      foreach ($subq as $i) {
        $q [] = $i;
      }
    }
    $fq = array_merge($fq, $subfq);
  }

  /**
   * @see apachesolr_filter::getActiveFilters()
   */
  function getActiveFilters() {
    $active = array();
    foreach ($this->filters as $name => $filter) {
      $tmp = $filter->getActiveFilters();
      $active[$name] = $tmp[$name];
    }
    return $active;
  }
}
