<?php


/**
 * Filters a Solr query based on a defined search page.
 * @param $query
 * @param $page
 */
function _apachesolr_pages_filter_query($query, $page) {
  list($q, $fq) = $page->buildQuery();
  $query->addParam('q', $q->getLucene(TRUE));
  $query->addParam('fq', $fq->asFieldQuery(TRUE));
}

/**
 * Builds a Solr query from a search page.
 * @param $solr
 * @param $page
 * @param int $ipp
 * @param int $from
 * @param array $params
 * @return \SolrBaseQuery
 */
function _apachesolr_pages_build_query($solr, $page, $ipp = 15, $from = 0, $params = array()) {
  $sortstring = '';
  if (!empty($params['sort'])) {
    if (is_array($params['sort'])) {
      $sortstring = implode(', ', $params['sort']);
    }
    else {
      $sortstring = $params['sort'];
    }
  }
  $query = new SolrBaseQuery('apachesolr', $solr, $params, $sortstring, current_path());
  if ($page) {
    apachesolr_search_basic_params($query);

    $query->addParam('fl', 'id,label,entity_id,entity_type,bundle');

    _apachesolr_pages_filters();
    _apachesolr_pages_filter_query($query, $page);

    if (!empty($_REQUEST['sort'])) {
      $sorts = explode(',', $_REQUEST['sort']);
      $orders = explode(',', $_REQUEST['order']);
      foreach ($sorts as $i => $s) {
        $sort = $page->getSort($s);
        $order = $orders[$i];
        $query->addParam('sort', $sort->getField() . ' ' . $order);
      }
    }
    else {
      $sorts = array();
      foreach ($page->getSorts() as $sort) {
        if ($sort->isDefault()) {
          $sorts[] = $sort;
        }
      }
      foreach ($sorts as $sort) {
        $query->addParam('sort', $sort->getField() . ' ' . $sort->getOrder());
      }
    }
    list($q, $fq) = $page->getFacetParams();
    if (!empty($q) || !empty($fq)) {
      $query->addParam('facet', 'true');
      $query->addParam('facet.field', $fq);
      $query->addParam('facet.query', $q);
    }
  }

  if ($ipp != -1) {
    $query->addParam('rows', $ipp);
  }
  else {
    $query->addParam('rows', 10000);
  }

  $query->addParam('start', $from);

  if ($page) {
    if ($page->getAllowAlter()) {
      drupal_alter('apachesolr_query', $query);
      apachesolr_search_highlighting_params($query);
      apachesolr_search_add_spellcheck_params($query);
    }
    $page->alterQuery($query);
    apachesolr_search_add_boost_params($query);
    if ($page->getAllowAlter()) {
    }
    else {
      drupal_alter('apachesolr_pages_query', $query);
    }
  }
  return $query;
}

/**
 * Returns the results of a Solr query, spellcheck and facets included.
 * @param $solr
 * @param $page
 * @param int $ipp
 * @param int $from
 * @param array $params
 * @return array
 */
function apachesolr_pages_search_results($solr, $page, $ipp = 15, $from = 0, $params = array()) {
  $build = array();
  $query = _apachesolr_pages_build_query($solr, $page, $ipp, $from, $params);

  if (!empty($query->halt)) {
    return array($query, NULL);
  }

  $res = $query->search();

  if ($ipp != -1) {
    pager_default_initialize($res->response->numFound, $ipp, 0);
  }

  if ($page) {
    $res->response->results = apachesolr_search_process_response($res, $query);
  }
  else {
    foreach ($res->response->docs as $i => $doc) {
      $res->response->results[$i]['fields'] = (array) $doc;
    }
  }
  return array($query, $res->response, @$res->spellcheck, $res->facet_counts);
}
