<?php

/**
 * Shorthand function for instantiating page classes.
 * @param $id
 * @return bool|object
 */
function as_pages_page($id) {
  $args = func_get_args();
  array_shift($args);
  $page_type = _apachesolr_pages_page_types($id);
  if ($page_type && isset($page_type['handler']) && class_exists($page_type['handler'])) {
    $reflect = new ReflectionClass($page_type['handler']);
    return $reflect->newInstanceArgs($args);
  }
  else {
    return FALSE;
  }
}

/**
 * Returns a cached list of all page types.
 * @param bool $id
 * @return array|bool
 */
function _apachesolr_pages_page_types($id = FALSE) {
  $page_types = &drupal_static(__FUNCTION__, FALSE);
  if (!$page_types) {
    $page_types = module_invoke_all('apachesolr_pages_page_types');
    module_load_include('inc', 'apachesolr_pages', 'page_types/apachesolr_page');
    foreach ($page_types as $key => &$page_type) {
      _apachesolr_pages_load_handler($key, $page_type);
    }
  }
  if ($id) {
    if (isset($page_types[$id])) {
      return $page_types[$id];
    }
    else {
      return FALSE;
    }
  }
  return $page_types;
}

/**
 * Implements hook_apachesolr_pages_page_types().
 */
function apachesolr_pages_apachesolr_pages_page_types() {
  $page_types = array(
    'entity_list' => array(
      'handler' => 'apachesolr_page_entity_list',
      'path' => drupal_get_path('module', 'apachesolr_pages') . '/page_types',
    ),
    'table' => array(
      'handler' => 'apachesolr_page_table',
      'path' => drupal_get_path('module', 'apachesolr_pages') . '/page_types',
    ),
  );
  return $page_types;
}
