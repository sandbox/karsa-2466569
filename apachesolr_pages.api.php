<?php
/**
 * @file
 * Hooks provided by the Apachesolr Pages module.
 * TODO. :)
 */


/**
 * Return a list of content types that could be added as headers and footers.
 */
function hook_apachesolr_pages_content_types() {

}

/**
 * Return a list of filter types.
 */
function hook_apachesolr_pages_filters() {

}

/**
 * Return a list of page types.
 */
function hook_apachesolr_pages_page_types() {

}

/**
 * Return a list of sort types.
 */
function hook_apachesolr_pages_sorts() {

}

/**
 * Return a list of defined search pages.
 */
function hook_apachesolr_pages_pages() {
  $pages['PAGE_ID'] = array(
    'name' => t('Custom search page'),
    'module' => 'MY_MODULE',
  );
  return $pages;
}

function hook_apachesolr_pages_page($id) {
  switch ($id) {
    case 'PAGE_ID':
      return as_pages_page('entity_list', 'custom_search', t('My custom search page'), 'custom-search')
        ->addMenuItem('search/custom', array(
          'type' => MENU_LOCAL_TASK,
          'weight' => -1,
        ))
        ->setSourceDomain('my_source_domain')
        ->setHasBlockForm(TRUE)
        ->setBlockTitle('Search')
        ->setRequireInput(FALSE)
        ->addAttached('js', 'my.js')
        ->addAttached('css', 'my.css')
        ->setIPP(10)
        ->addFilter('keys', as_pages_filter('text', NULL, t('Search'))
          ->setOmitActiveFilterTitle(TRUE)
          ->setPlaceholder(TRUE)
          ->setExposed(TRUE)
        )
        ->addArgument('bundle', 0, as_pages_filter('text', 'bundle'))
        ->setParams(array(
          'fq' => array(
            'entity_type:node',
            'bs_status:true'
          ),
          'sort' => array('ds_created desc'),
        ));
  }
}