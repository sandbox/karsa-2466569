<div<?php echo $attributes; ?>>
  <div class="apachesolr-exposed-form-wrapper"><?php echo render($form); ?></div>
  <div class="clear"></div>
  <div class="suggestions"><?php echo $suggestions; ?></div>
  <div class="clear"></div>
  <div class="headers"><?php echo $headers; ?></div>
  <div class="clear"></div>
  <div class="apachesolr-results clearfix">
    <?php if (!empty($active_filters)): ?>
      <div class="apachesolr-active-filters"><?php echo render($active_filters); ?></div>
    <?php endif; ?>
    <?php if ($results['pager']): ?>
      <div class="apachesolr-pager apachesolr-pager-top"><?php echo render($results['pager']); ?></div>
      <div class="clear"></div>
    <?php endif; ?>
    <div class="clear"></div>
    <div class="apachesolr-results"><?php echo render($results['results']); ?></div>
    <div class="clear"></div>
    <?php if ($results['pager']): ?>
      <div class="apachesolr-pager apachesolr-pager-bottom"><?php echo render($results['pager']); ?></div>
      <div class="clear"></div>
    <?php endif; ?>
  </div>
  <div class="clear"></div>
  <div class="footers"><?php echo $footers; ?></div>
  <div class="clear"></div>
</div>
