<div class="<?php echo $results_classes; ?>">
  <?php foreach ($items as $item): ?>
    <div class="apachesolr-row <?php echo $item['class']; ?>">
      <?php echo $item['content']; ?>
    </div>
  <?php endforeach; ?>
</div>
