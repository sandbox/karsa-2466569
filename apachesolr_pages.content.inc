<?php
/**
 * @file
 * Apachesolr Pages custom content types.
 */

/**
 * Shorthand function for instantiating content objects.
 * @param $id
 * @return null|object
 */
function as_pages_content($id) {
  $args = func_get_args();
  array_shift($args);
  $content_type = _apachesolr_pages_content_types($id);
  if ($content_type && isset($content_type['handler']) && class_exists($content_type['handler'])) {
    $reflect = new ReflectionClass($content_type['handler']);
    return $reflect->newInstanceArgs($args);
  }
  else {
    return NULL;
  }
}


/**
 * Returns a cached list of all content types.
 * @param bool $id
 * @return array|null
 */
function _apachesolr_pages_content_types($id = FALSE) {
  $content_types = &drupal_static(__FUNCTION__, FALSE);
  if (!$content_types) {
    $content_types = module_invoke_all('apachesolr_pages_content_types');
    module_load_include('inc', 'apachesolr_pages', 'content/apachesolr_content');
    foreach ($content_types as $key => &$content_type) {
      _apachesolr_pages_load_handler($key, $content_type);
    }
  }
  if ($id) {
    if (isset($content_types[$id])) {
      return $content_types[$id];
    }
    else {
      return NULL;
    }
  }
  return $content_types;
}

/**
 * Implements hook_apachesolr_pages_content_types().
 */
function apachesolr_pages_apachesolr_pages_content_types() {
  $content_types = array(
    'text' => array(
      'handler' => 'apachesolr_content_text',
      'path' => drupal_get_path('module', 'apachesolr_pages') . '/content',
    ),
  );
  return $content_types;
}
