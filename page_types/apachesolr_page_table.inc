<?php

/**
 * A simple table based search page.
 */
class apachesolr_page_table extends apachesolr_page {
  /**
   * The theme function to use (@see apachesolr_page::theme).
   * @var string
   */
  var $theme = 'apachesolr_pages_page_table';
  /**
   * The results theme (one isn't needed) (@see apachesolr_page::results_theme).
   * @var bool
   */
  var $results_theme = FALSE;
  /**
   * The fields to display in the table.
   * @var array
   */
  var $fields = array();
  /**
   * The table headers.
   * @var array
   */
  var $tableheaders = array();
  /**
   * Formatters for certain fields key by field key.
   * @var array
   */
  var $formatters = array();

  /**
   * Adds a field to the table for display.
   * @param $key
   * @param $field
   * @param $title
   * @param bool $formatter
   * @param array $formatter_options
   * @return $this
   */
  function addField($key, $field, $title, $formatter = FALSE, $formatter_options = array()) {
    $this->fields[$key] = $field;
    $this->tableheaders[$key] = $title;
    if ($formatter) {
      if (is_array($formatter)) {
        $this->formatters[$key] = $formatter['formatter'];
        $this->formatter_options[$key] = $formatter;
      }
      else {
        $this->formatters[$key] = $formatter;
        $this->formatter_options[$key] = $formatter_options;
      }
    }
    else {
      $this->formatters[$key] = '_apachesolr_pages_field_value';
      $this->formatter_options[$key] = $formatter_options;
    }
    return $this;
  }

  /**
   * @see apachesolr_page::renderRow()
   */
  function renderRow($data) {
    $row = array();
    foreach ($this->fields as $key => $field) {
      $row[$key] = $this->formatters[$key]($field, $data['fields'], $this->formatter_options[$key]);
    }
    return $row;
  }

  /**
   * @see apachesolr_page::alterBuild()
   */
  function alterBuild(&$build) {
    $build['tableheaders'] = $this->buildHeaders();
    $build['#fields'] = $this->fields;
  }

  /**
   * Builds the table headers with sort buttons included.
   * @return array
   */
  function buildHeaders() {
    $tableheaders = array();
    foreach ($this->tableheaders as $key => $tableheader) {
      if ($sort = $this->getSort($key)) {
        $tableheaders[$key] = apachesolr_pages_sort_link($this, $tableheader, $key, $sort);
      }
      else {
        $tableheaders[$key] = array('content' => $tableheader);
      }
      if (empty($tableheaders[$key]['class'])) {
        $tableheaders[$key]['class'] = array();
      }
      $tableheaders[$key]['class'][] = 'field-' . $key;

      $tableheaders[$key]['class'] = implode(' ', $tableheaders[$key]['class']);
    }
    return $tableheaders;
  }

  /**
   * @see apachesolr_page::alterQuery()
   */
  function alterQuery($query) {
    if (!empty($this->fields)) {
      $query->addParam('fl', implode(',', $this->fields));
    }
  }
}

/**
 * Returns the field value for a Solr field.
 * @param $field
 * @param $data
 * @param $options
 * @return string
 */
function _apachesolr_pages_field_value($field, $data, $options) {
  return (@$options['prefix']) . $data[$field] . (@$options['suffix']);
}

/**
 * Renders a Solr field, formatted as a link to the entity.
 * @param $field
 * @param $data
 * @param $options
 * @return string
 */
function _apachesolr_pages_linked_field($field, $data, $options) {
  $entities = &drupal_static('apachesolr_pages_entities', array());
  if (empty($entities[$data['entity_type']])) {
    $entities[$data['entity_type']] = array();
  }
  if (empty($entities[$data['entity_type']][$data['entity_id']])) {
    $entities[$data['entity_type']][$data['entity_id']] = entity_load_single($data['entity_type'], $data['entity_id']);
  }
  $uri = entity_uri($data['entity_type'], $entities[$data['entity_type']][$data['entity_id']]);
  return l($data[$field], $uri['path']);
}

/**
 * Renders an entity field, based on the Solr row.
 * @param $field
 * @param $data
 * @param $options
 * @return string
 */
function _apachesolr_pages_render_field($field, $data, $options) {
  $entities = &drupal_static('apachesolr_pages_entities', array());
  if (empty($entities[$data['entity_type']])) {
    $entities[$data['entity_type']] = array();
  }
  if (empty($entities[$data['entity_type']][$data['entity_id']])) {
    $entities[$data['entity_type']][$data['entity_id']] = entity_load_single($data['entity_type'], $data['entity_id']);
  }
  $field_view = field_view_field($data['entity_type'], $entities[$data['entity_type']][$data['entity_id']], $options['field_name'], $options['display']);
  return render($field_view);
}

/**
 * Renders a date field.
 * @param $field
 * @param $data
 * @param $options
 * @return bool|mixed|string
 */
function _apachesolr_pages_date_field($field, $data, $options) {
  if (isset($options['format'])) {
    $options['mode'] = 'custom';
  }
  if (empty($options['mode'])) {
    $options['mode'] = 'medium';
  }
  if (empty($options['format'])) {
    return date_format(strtotime($data[$field]), $options['mode']);
  }
  $time = strtotime($data[$field]);
  return format_date($time, $options['mode'], $options['format']);
}
