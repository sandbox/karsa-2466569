<?php

/**
 * A simple entity list with entities rendered by view mode.
 */
class apachesolr_page_entity_list extends apachesolr_page {
  /**
   * A custom view mode to render the entities in.
   * @var string
   */
  var $view_mode = 'search_result';

  /**
   * @see apachesolr_page::renderRow().
   */
  function renderRow($data) {
    $entity = entity_load_single($data['fields']['entity_type'], $data['fields']['entity_id']);
    $entity->search_snippet = @$data['snippet'];
    $view = entity_view($data['fields']['entity_type'], array($entity), $this->getViewMode());
    return render($view);
  }

  /**
   * Sets a view mode for the entity list items.
   * @param $view_mode
   * @return $this
   */
  function setViewMode($view_mode) {
    $this->view_mode = $view_mode;
    return $this;
  }

  /**
   * Returnt the set view mode.
   */
  function getViewMode() {
    return $this->view_mode;
  }

}
