(function ($) {
  Drupal.asp = Drupal.asp || {};
  $.connectCheckboxes = function ($title, $options, xor) {
    $options.change(function () {
      if ($options.filter(':checked').length == $options.length) {
        if (xor !== true || $options.length != 1) $title.prop('checked', true);
        if (xor === true && $options.length != 1) $options.prop('checked', false);
      } else {
        $title.prop('checked', false);
      }
    });
    $title.change(function () {
      if ($title.is(':checked') && xor !== true) {
        $options.filter(':not(:disabled)').prop('checked', true);
      } else {
        $options.filter(':not(:disabled)').prop('checked', false);
      }
    });
  };

  $.fn.groupSelector = function () {
    return this.each(function () {
      var $widget = $(this);
      var $title = $('.asp-option-group-title input[type="checkbox"]', $widget);
      var $options = $('.asp-options input[type="checkbox"]', $widget);
      $.connectCheckboxes($title, $options, true);
    });
  };

  $.fn.modalOptions = function () {
    return this.each(function () {
      var $widget = $(this);
      var $button = $('.asp-more-button', $widget);
      var $modalOptions = $widget.data('modalform');
      var $limit = $widget.data('limit');
      var $cutoffLimit = $widget.data('cutoff-limit');
      var $form = $widget.closest('form');
      var $checkboxes = $('input[type="checkbox"], input[type="radio"]', $widget);
      $widget.updateCheckboxes($cutoffLimit, $limit);
      $checkboxes.change(function () {
        $widget.updateCheckboxes($cutoffLimit, $limit);
      });
      $button.click(function (e) {
        e.preventDefault();
        if (Drupal.settings.asp.ajax) return;
        Drupal.asp.modalContents = $('.asp-modal-contents', Drupal.asp.modal);
        Drupal.asp.modalContents.empty();
        Drupal.asp.modalContents.append($modalOptions);
        Drupal.attachBehaviors(Drupal.asp.modalContents);
        $widget.copyState(Drupal.asp.modalContents, $cutoffLimit, $limit);
        var $cancelbutton = $('.asp-cancel-button', Drupal.asp.modalContents);
        var $okbutton = $('.asp-ok-button', Drupal.asp.modalContents);
        Drupal.asp.modal.fadeIn();
        $.event.trigger('aspModalOpen', [$widget, $cutoffLimit, $limit]);
        $cancelbutton.click(function (e) {
          $.closeModal(e, $widget, $cutoffLimit, $limit)
        });
        $('.asp-modal-overlay', Drupal.asp.modal).click(function (e) {
          $.closeModal(e, $widget, $cutoffLimit, $limit)
        });
        $okbutton.click(function (e) {
          e.preventDefault();
          Drupal.asp.modalContents.copyState($widget, $cutoffLimit, $limit, true);
          $.event.trigger('aspFormChange');
          $.closeModal(e, $widget, $cutoffLimit, $limit);
        });
      });
    });
  };

  $.fn.extendOptions = function () {
    return this.each(function () {
      var $wrapper = $(this);
      var $button = $('.asp-more-button', $wrapper);
      var $widget = $('.asp-options-element', $wrapper);
      var $modalOptions = $widget.data('modalform');
      var $cutoffLimit = $widget.data('cutoff-limit');
      var $limit = $widget.data('limit');
      var $form = $widget.closest('form');
      var $checkboxes = $('input[type="checkbox"], input[type="radio"]', $widget);
      // TODO
    });
  };

  $.fn.showOption = function () {
    this.closest('.asp-option').show();
    return this;
  };
  $.fn.hideOption = function () {
    this.closest('.asp-option').hide();
    return this;
  };

  $.fn.updateCheckboxes = function ($cutoffLimit, $limit) {
    var $checkboxes = $('input[type="checkbox"], input[type="radio"]', this);
    if ($checkboxes.length > $cutoffLimit) {
      var $checked = $checkboxes.filter(':checked').length;
      $('.asp-more-button', this.closest('.asp-options-element')).toggle($checkboxes.length != $checked);
      $checkboxes.filter(':checked').showOption();
      $checkboxes.filter(':not(:checked)').hideOption();
      if ($checked < $cutoffLimit) {
        var $remainder = $limit - $checked;
        var $uncheckedBase = $checkboxes.filter('.prioritized').filter(':not(:checked)').slice(0, $remainder).showOption();
        $remainder = $remainder - $uncheckedBase.length;
        if ($remainder > 0) {
          var $yieldsResult = $checkboxes.filter('[data-facet!="+0"]:not(:checked)').slice(0, $remainder).showOption();
          $remainder = $remainder - $yieldsResult.length;
          if ($remainder > 0) {
            $checkboxes.filter('[data-facet="+0"]:not(:checked)').slice(0, $remainder).showOption();
          }
        }
      }
    }
    return this;
  };
  $.fn.copyState = function ($to, $cutoffLimit, $limit, $addClasses) {
    $from = this;
    var $cnt = $('*[data-key]:checked', $from).length;
    $('*[data-key]', $from).each(function () {
      var $source = $(this);
      var $key = $source.data('key');
      var $target = $('*[data-key="' + $key + '"]', $to);
      $source.prop('checked', $source.is(':checked'));
      $target.prop('checked', $source.is(':checked'));
      $source.prop('disabled', $source.is(':disabled'));
      $target.prop('disabled', $source.is(':disabled'));
      if ($addClasses == true) {
        $to.updateCheckboxes($cutoffLimit, $limit);
      }
    });
    return this;
  };
  $.closeModal = function (e, $widget, $cutoffLimit, $limit) {
    e.preventDefault();
    Drupal.asp.modal.fadeOut();
    $.event.trigger('aspModalClose');
  };

  $(document).bind('aspAjaxUpdate', function (e, $data, selector, formSelector) {
    var $form = $(formSelector, document);
    var formContent = $(formSelector, $data);
    $('*[data-modalform]', formContent).each(function () {
      var $modalItem = $(this);
      $form.find('*[data-modal-id="' + $modalItem.data('modal-id') + '"]').data('modalform', $modalItem.data('modalform'));
    });

    $('*[data-key]', $form).prop('disabled', false);

    $('*[data-key]', formContent).each(function () {
      var $source = $(this);
      var $key = $source.data('key');
      var $target = $('*[data-key="' + $key + '"]', $form);
      $source.prop('disabled', $source.is(':disabled'));
      $target.prop('disabled', $source.is(':disabled'));
//        $('.asp-options-modal', $form).updateCheckboxes();
    });

  });

  Drupal.behaviors.apachesolrPages = {
    attach: function (context, settings) {
      Drupal.asp.modal = $('.asp-modal');
      Drupal.asp.modalContents = $('.asp-modal-contents', Drupal.asp.modal);
      $('.allNone.asp-options', context).allNone();
      $('.asp-option-group', context).groupSelector();
      $('.asp-options-modal', context).modalOptions();
      $('.asp-options-extend', context).extendOptions();
    }
  };
})(jQuery);
