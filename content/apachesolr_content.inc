<?php

/**
 * Apachesolr Pages content handler class for header and footer items.
 */
abstract class apachesolr_content {
  /**
   * The administrative title of the content.
   * @var bool
   */
  var $title = FALSE;
  /**
   * The HTML content of the content.
   * @var bool
   */
  var $content = FALSE;
  /**
   * The text format of the content.
   * @var bool
   */
  var $format = FALSE;
  /**
   * Additional CSS classes to add to the content wrapper.
   * @var array
   */
  var $class = array('apachesolr-content');

  /**
   * @param bool $content
   * @param bool $format
   * @param bool $title
   */
  function __construct($content = FALSE, $format = FALSE, $title = FALSE) {
    $this->title = $title;
    $this->content = $content;
    $this->format = $format;
  }

  /**
   * Returns additional CSS classes.
   * @return array
   */
  function getClass() {
    return $this->class;
  }

  /**
   * Sets the additional CSS classes.
   * @param $class
   * @return $this
   */
  function setClass($class) {
    if (!is_array($class)) {
      $class = array($class);
    }
  }

  /**
   * Adds a class to the additional CSS classes.
   * @param $class
   * @return $this
   */
  function addClass($class) {
    if (is_array($class)) {
      $this->class = array_merge($this->class, $class);
    }
    else {
      $this->class[] = $class;
    }
    return $this;
  }

  /**
   * Returns the additional HTML attributes.
   * @return array
   */
  function getAttributes() {
    $attributes = $this->attributes;
    if (empty($attributes['class'])) {
      $attributes['class'] = array();
    }
    if (!is_array($attributes['class'])) {
      $attributes['class'] = array($attributes['class']);
    }
    $attributes['class'] = array_merge($attributes['class'], $this->getClass());
    return $attributes;
  }

  /**
   * Sets the additional HTML attributes.
   * @param $attributes
   * @return $this
   */
  function setAttributes($attributes) {
    $this->attributes = $attributes;
    return $this;
  }

  /**
   * Returns the formatted content.
   * @param bool $results
   * @return mixed
   */
  abstract function getFormattedContent($results = FALSE);
}
