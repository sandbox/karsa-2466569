<?php

/**
 * Simple formatted text content type.
 */
class apachesolr_content_text extends apachesolr_content {
  /**
   * @see apachesolr_content::getFormattedContent()
   */
  function getFormattedContent($results = FALSE) {
    return $this->content;
  }
}
