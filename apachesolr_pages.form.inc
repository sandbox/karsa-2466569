<?php


/**
 * Adds filters and submits to the search form.
 * @param $form
 * @param $form_state
 * @param $page
 * @return array
 */
function apachesolr_pages_search_form($form, $form_state, $page) {
  $page->addFiltersToForm($form);
  $page->addSubmitsToForm($form);
  $module_path = drupal_get_path('module', 'apachesolr_pages');
  $form += array(
    '#method' => 'GET',
    '#action' => url($page->getPath()),
    '#attached' => array(
      'css' => array(
        $module_path . '/apachesolr_pages.css',
      ),
      'js' => array(
        $module_path . '/jquery.allNone.js',
        $module_path . '/apachesolr_pages.js',
      ),
    ),
    '#attributes' => array('class' => array('apachesolr-exposed-form')),
  );
  return $form;
}

