<?php

/**
 * Simple sort handler class for search pages.
 */
class apachesolr_sort {
  /**
   * The field to base the sort on.
   * @var bool
   */
  var $field = FALSE;
  /**
   * The label to use for sorting.
   * @var bool
   */
  var $title = FALSE;
  /**
   * The default order to sort by.
   * @var bool|string
   */
  var $order = FALSE;
  /**
   * Whether this is the default sort of the page.
   * @var bool
   */
  var $default = FALSE;
  /**
   * Whether this sorter is exposed.
   * TODO: expose it.
   * @var bool
   */
  var $exposed = TRUE;

  /**
   * @param bool $field
   * @param bool $title
   * @param string $order
   * @param bool $exposed
   */
  function __construct($field = FALSE, $title = FALSE, $order = "asc", $exposed = TRUE) {
    $this->field = $field;
    $this->title = $title;
    $this->order = $order;
    $this->exposed = $exposed;
  }

  /**
   * Getter function for the field attribute.
   * @return bool
   */
  function getField() {
    return $this->field;
  }

  /**
   * Setter function for the field attribute.
   * @param $field
   * @return $this
   */
  function setField($field) {
    $this->field = $field;
    return $this;
  }

  /**
   * Getter function for the order attribute.
   * @return bool|string
   */
  function getOrder() {
    return $this->order;
  }

  /**
   * Setter function for the order attribute.
   * @param $order
   * @return $this
   */
  function setOrder($order) {
    $this->order = $order;
    return $this;
  }

  /**
   * Getter function for the default attribute.
   * @return bool
   */
  function isDefault() {
    return $this->default;
  }

  /**
   * Setter function for the default attribute.
   * @param $default
   * @return $this
   */
  function setDefault($default) {
    $this->default = $default;
    return $this;
  }

  /**
   * Getter function for the title attribute.
   * @return bool
   */
  function getTitle() {
    return $this->title;
  }

  /**
   * Setter function for the title attribute.
   * @param $title
   * @return $this
   */
  function setTitle($title) {
    $this->title = $title;
    return $this;
  }

  /**
   * Getter function for the exposed attribute.
   * @return bool
   */
  function isExposed() {
    return $this->exposed;
  }

  /**
   * Setter function for the exposed attribute.
   * @param $exposed
   * @return $this
   */
  function setExposed($exposed) {
    $this->exposed = $exposed;
    return $this;
  }
}
