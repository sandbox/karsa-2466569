<?php
/**
 * @file
 * Block hooks and helper functions
 */


/**
 * Implements hook_block_info().
 */
function apachesolr_pages_block_info() {
  $blocks['__current_page__'] = array(
    'info' => t('Apache Solr Pages search form: current search page'),
    'cache' => DRUPAL_NO_CACHE,
  );
  foreach (_apachesolr_pages_pages() as $page_id => $page_info) {
    $page = apachesolr_pages_get_page($page_id);
    if ($page->getHasBlockForm()) {
      $blocks[$page->getId()] = array(
        'info' => t('Apache Solr Pages search form: @name', array('@name' => $page->getTitle())),
        'id' => $page->getId(),
        'cache' => DRUPAL_NO_CACHE,
      );
    }
  }
  return $blocks;
}


/**
 * Implements hook_block_view().
 */
function apachesolr_pages_block_view($delta = '') {
  $block = array();
  if ($delta == '__current_page__') {
    $menuitem = menu_get_item();
    $id = @$menuitem['page_arguments'][0];
  }
  else {
    $id = str_replace('asp_', '', $delta);
  }
  if (!$id) {
    return $block;
  }
  $page = apachesolr_pages_get_page($id);
  if ($page) {
    if ($delta == '__current_page__' && !$page->getHasBlockForm()) {
      return $block;
    }
    $block = array(
      'content' => $page->getForm(TRUE),
      'subject' => $page->getBlockTitle() ? $page->getBlockTitle() : t('Filter'),
    );
  }
  return $block;
}

/**
 * Implements hook_preprocess_HOOK().
 */
function apachesolr_pages_preprocess_block(&$variables) {
  if ($variables['block_html_id'] == 'block-apachesolr-pages-current-page-') {
    $menuitem = menu_get_item();
    $id = @$menuitem['page_arguments'][0];
    if (!$id) {
      return;
    }
    $page = apachesolr_pages_get_page($id);
    $variables['classes_array'][] = 'block-asp-' . $id;
  }
}