<?php

/**
 * Shorthand function for instantiating sort objects.
 * @param $id
 * @return bool|object
 */
function as_pages_sort($id) {
  $args = func_get_args();
  array_shift($args);
  $sort = _apachesolr_pages_sorts($id);
  if ($sort && isset($sort['handler']) && class_exists($sort['handler'])) {
    $reflect = new ReflectionClass($sort['handler']);
    return $reflect->newInstanceArgs($args);
  }
  else {
    return FALSE;
  }
}

/**
 * Returns a cached list of all sorts.
 * @param bool $id
 * @return array|bool
 */
function _apachesolr_pages_sorts($id = FALSE) {
  $sorts = &drupal_static(__FUNCTION__, FALSE);
  if (!$sorts) {
    $sorts = module_invoke_all('apachesolr_pages_sorts');
    module_load_include('inc', 'apachesolr_pages', 'sorts/apachesolr_sort');
    foreach ($sorts as $key => &$sort) {
      _apachesolr_pages_load_handler($key, $sort);
    }
  }
  if ($id) {
    if (isset($sorts[$id])) {
      return $sorts[$id];
    }
    else {
      return FALSE;
    }
  }
  return $sorts;
}

/**
 * Implements hook_apachesolr_pages_sorts().
 */
function apachesolr_pages_apachesolr_pages_sorts() {
  $sorts = array(
    'sort' => array(
      'handler' => 'apachesolr_sort',
      'path' => drupal_get_path('module', 'apachesolr_pages') . '/sorts',
    ),
  );
  return $sorts;
}

/**
 * Creates a sort link for a sort object from a search page.
 * @param $page
 * @param $title
 * @param $key
 * @param $sort
 * @return array
 */
function apachesolr_pages_sort_link($page, $title, $key, $sort) {
  $query = drupal_get_query_parameters();
  $s = @$_REQUEST['sort'];
  $o = @$_REQUEST['order'];
  if ($s) {
    $s = explode(',', $s);
  }
  else {
    $s = array();
  }
  if ($o) {
    $o = explode(',', $o);
  }
  else {
    $o = array();
  }
  $order = "asc";
  $current = FALSE;
  foreach ($s as $i => $c) {
    if ($key == $c) {
      if ($o[$i] == "asc") {
        $order = "desc";
      }
      else {
        $order = FALSE;
      }
      $current = $o[$i];
      unset($s[$i]);
      unset($o[$i]);
    }
  }
  if ($order) {
    array_unshift($s, $key);
    array_unshift($o, $order);
  }
  $query['sort'] = implode(',', $s);
  $query['order'] = implode(',', $o);
  unset($query['q']);

  array_filter($query);

  return array(
    'content' => l($title, current_path(), array(
      'query' => $query,
      'attributes' => array('class' => array($current))
    )),
    'order' => $current
  );
}
