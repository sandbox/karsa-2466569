(function ($) {
  $(document).ready(function () {
    Drupal.settings.asp = Drupal.settings.asp || {};
    $('.apachesolr-date-interval').each(function () {
      var $i = $(this);
      var $from = $('.from', $i);
      var $to = $('.to', $i);
      var $from_y = $('select.year', $from);
      var $from_m = $('select.month', $from);
      var $from_d = $('select.day', $from);
      var $to_y = $('select.year', $to);
      var $to_m = $('select.month', $to);
      var $to_d = $('select.day', $to);

      $('option', $to_y).each(function () {
        var $o = $(this);
        $o.prop('disabled', parseInt($from_y.val()) > parseInt($o.val()) && $o.val() != 0);
      });
      $('option', $to_m).each(function () {
        if ($from_y.val() == $to_y.val()) {
          var $o = $(this);
          $o.prop('disabled', parseInt($from_m.val()) > parseInt($o.val()));
        } else {
          $o.prop('disabled', false);
        }
      });
      $('option', $to_d).each(function () {
        if ($from_y.val() == $to_y.val() && $from_m.val() == $to_m.val()) {
          var $o = $(this);
          $o.prop('disabled', parseInt($from_d.val()) > parseInt($o.val()));
        } else {
          $o.prop('disabled', false);
        }
      });
      $to_y.trigger("chosen:updated");
      $to_m.trigger("chosen:updated");
      $to_d.trigger("chosen:updated");
    });
    $('.apachesolr-date-interval select').live('change', function () {
      var $t = $(this);
      var $i = $t.parents('.apachesolr-date-interval');
      var $from = $('.from', $i);
      var $to = $('.to', $i);
      var $from_y = $('select.year', $from);
      var $from_m = $('select.month', $from);
      var $from_d = $('select.day', $from);
      var $to_y = $('select.year', $to);
      var $to_m = $('select.month', $to);
      var $to_d = $('select.day', $to);

      if ($t.parents('.from').length > 0) {
        $to_y.val($from_y.val());
        $to_m.val($from_m.val());
        $to_d.val($from_d.val());
      }

      var yearset = $from_y.val() != 0;
      var year2set = $to_y.val() != 0;

      $from_m.prop('disabled', !yearset);
      $from_d.prop('disabled', !yearset);
      $to_m.prop('disabled', !year2set);
      $to_d.prop('disabled', !year2set);

      $('option', $to_y).each(function () {
        var $o = $(this);
        $o.prop('disabled', parseInt($from_y.val()) > parseInt($o.val()) && $o.val() != 0);
      });
      $('option', $to_m).each(function () {
        if ($from_y.val() == $to_y.val()) {
          var $o = $(this);
          $o.prop('disabled', parseInt($from_m.val()) > parseInt($o.val()));
        } else {
          $o.prop('disabled', false);
        }
      });
      $('option', $to_d).each(function () {
        if ($from_y.val() == $to_y.val() && $from_m.val() == $to_m.val()) {
          var $o = $(this);
          $o.prop('disabled', parseInt($from_d.val()) > parseInt($o.val()));
        } else {
          $o.prop('disabled', false);
        }
      });
      $from_y.trigger("chosen:updated");
      $from_m.trigger("chosen:updated");
      $from_d.trigger("chosen:updated");
      $to_y.trigger("chosen:updated");
      $to_m.trigger("chosen:updated");
      $to_d.trigger("chosen:updated");
    });

    $('.apachesolr-exposed-form').each(function () {
      var $form = $(this);

      $(document).bind('aspFormChange', function (e) {
        $form.submit();
      });

      var ajaxUpdate = function (params, method) {
        method = method || 'GET';
        if (Drupal.settings.asp.skipDefaultAjax) {
          return;
        }

        // TODO: unique selector for multiple lists on one page
        var selector = '.apachesolr-page';
        var formSelector = '#' + $form.attr('id');

        var params2 = params;

        $.ajax(window.location.pathname, {
          type: method,
          cache: false,
          dataType: 'html',
          data: params,
          timeout: 15000, // milliseconds
          beforeSend: function (jqXHR) {
            $(selector).fadeTo(500, 0.5);
            Drupal.settings.asp.ajax = true;
          },
          success: function (data) {
            $.event.trigger('aspAjaxUpdate', [data, selector, formSelector]);
            var formContent = $(data).find(formSelector);
            $('.asp-facet-info', formContent).each(function () {
              var $facet = $(this);
              $form.find('.asp-facet-info[data-facet-id="' + $facet.data('facet-id') + '"]').html($facet.html());
            });
            var ajaxContent = $(data).find(selector).children();
            $(selector).html(ajaxContent).fadeTo(500, 1);
            Drupal.attachBehaviors($(selector));
            var stateObj = {foo: "bar"};
            history.pushState(stateObj, "page", window.location.pathname + '?' + params2);
            Drupal.settings.asp.ajax = false;
          },
          error: function () {
            $(selector).slideUp();
            $(selector).fadeTo(500, 1);
          }
        });
      };
      // Reset button
      $('input.reset-button', $form).click(function (event) {
        if ($form.parents('.map').length == 0) {
        }
        event.preventDefault();

        if (true || $(this).hasClass('ajax')) {
          // textfields
          $('input[type=text]', $form).each(function () {
            $(this).val('');
          });
          // checkboxes
          $('input[type=checkbox]', $form).each(function () {
            $(this).prop('checked', '');
          });
          // selects
          $('select:not(:has(.apachesolr-filters-hierarchical-helper))', $form).each(function () {
            $(this)
              .find('option:first').attr('selected', 'selected').end()
              .change();
          });
          // hierarchical selects
          $('div.form-item:has(.apachesolr-filters-hierarchical-helper)', $form).each(function () {
            $('.apachesolr-filters-hierarchical-helper:eq(0)', this).
              find('option:first').attr('selected', 'selected').end()
              .siblings().remove();
          });

          ajaxUpdate('');
        } else {
          window.location.href = window.location.pathname;
        }
      });

      $form.find('input[data-target]').click(function (event) {
        event.preventDefault();
        var $c = $(this);
        $form = $c.parents("form");
        var $orig = $form.attr("action");
        $form.attr("action", $c.data("target"));
        $form.redirect = $c.data("target");
        $.event.trigger('aspFormChange');
        $form.attr("action", $orig);
      });

      // Basic submit
      $form.submit(function (event, elem) {
        if ($form.redirect) return;
        event.preventDefault();
        $('input[name="form_id"], input[name="form_build_id"], input[name="form_token"]', $form).remove();
        ajaxUpdate($form.serialize());
      });

      var timeoutID;
      $form.find('input[type="checkbox"], input[type="radio"]').change(function (event) {
        clearTimeout(timeoutID);
        timeoutID = setTimeout(function () {
          $.event.trigger('aspFormChange');
        }, 500);
      });

      // Pager buttons.
      $('.apachesolr-pager a, .apachesolr-active-filters a').live('click', function (event) {
        var $link = $(this);
        clearTimeout(timeoutID);
        timeoutID = setTimeout(function () {
          event.preventDefault();
          var params = $link.attr('href').match(/\?(.*)/)[1]; // A link ? utáni rész
          if (params) {
            ajaxUpdate(params, 'GET'); // GET, hogy ne menjen át a form post procedúrán
          }
        }, 500);
      });

      // Hierarchical lists.
      $('.apachesolr-filters-hierarchical').each(function () {
        var $hidden = $(this);
        var ajaxPath = Drupal.settings.basePath + 'ajax/apachesolr-pages/hierarchical/' + $hidden.data('vid');
        var dontmodify = false;
        var preselect = {};
        preselect[$hidden.attr("name")] = $hidden.data('tid');
        var options = {
          empty_value: '',
          weighted: true, // the data in tree is indexed by weights instead of ids, the ids and labels are stored in the value as an associative object
          indexed: true,  // the data in tree is indexed by values (ids), not by labels
          on_each_change: ajaxPath, // this file will be called with 'id' parameter, JSON data must be returned
          set_value_on: 'each', // we will change input value when every select box changes
          choose: Drupal.t('- Please select -'),
          select_class: 'apachesolr-filters-hierarchical-helper',
          preselect: preselect
        };
        $.getJSON(ajaxPath, function (tree) { // initialize the tree by loading the file first
          $hidden.optionTree(tree, options).change(function (e, f) {
            if ($(this).val() == '-1') {
              $(this).val($(this).prev().prev().val()); // on reset write back the parent id.
            }
          });
        });
      });
    });
  });

})(jQuery);
